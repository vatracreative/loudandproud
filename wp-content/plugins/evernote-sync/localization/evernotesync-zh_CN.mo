��    %      D  5   l      @     A     S     a     t  .   �     �     �     �     �  	   �  $   �          *  �   ;  	   /     9  	   E     O     R     [     h     u     �     �     �     �     �     �     �     �     �     �  	             %     @  �  \          .     ;     N  *   ]     �     �     �     �     �  ,   �     �     	    	     *
     1
     >
     Q
     U
     \
     i
     v
     �
     �
     �
     �
     �
     �
     �
     �
                #     *     1      Q                  "                           	                           $                                        %   
              !                              #                    Clear Sync Record Clear success Count of Each Sync Developer Token Do you really want to delete all sync records? Donate Evernote EvernoteSync EvernoteSync Plugin Options Every Day Explain: Sync with "posts" tag notes Format Content Generate Excerpt Get a developer token from: <a href="https://www.evernote.com/api/DeveloperToken.action" target="_blank">Evernote Developer Token</a> or <a href="https://app.yinxiang.com/api/DeveloperToken.action" target="_blank">Yinxiang Developer Token</a>. Home Page Manual Sync Next time No Platform Publish Mode Publish Time Save Options Save success. Sync success Tag for Sync The last synchronization time Time format is error! Yes Yinxiang draft e.g. invalid format for Sync Count! published timed use EverNote creation time use WordPress creation time Project-Id-Version: QuickTagDialog 1.0.0
Report-Msgid-Bugs-To: http://www.yongdui.com/EvernoteSync/
POT-Creation-Date: 
PO-Revision-Date: 2016-07-12 23:05+0800
Last-Translator: tanggaowei <tanggaowei@foxmail.com>
Language-Team: tanggaowei(http://yongdui.com) <tanggaowei@foxmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.8.8
Language: zh_CN
 清空同步记录 清空成功 每次同步总数 开发者Token 你确定要删除所有同步记录吗？ 捐赠 Evernote Evernote 同步 Evernote 同步插件选项 每天 说明：同步带“posts”标签的笔记 格式化内容 生成摘要 从后面的链接页面获取 Developer Token from：<a href="https://www.evernote.com/api/DeveloperToken.action" target="_blank">Evernote 开发者Token</a> 或者 <a href="https://app.yinxiang.com/api/DeveloperToken.action" target="_blank">印象笔记开发者Token</a>。 首页 手动同步 下次同步时间 否 平台 发布方式 发布时间 保存选项 保存成功。 同步成功 用于同步的标签 最后一次同步时间 时间格式不正确！ 是 印象笔记 草稿 例如 同步总数格式无效！ 发布 定时 使用 Evernote 的创建时间 使用 WordPress 的创建时间 