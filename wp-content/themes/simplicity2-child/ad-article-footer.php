<?php if ( is_ads_visible() ): //広告表示がオンのとき?>
  <!-- 文章下広告 -->
    <?php if (is_mobile()): //スマートフォンの場合?>

         <div class="ad__space--bottom">
          <div class="ad-label"><?php echo get_ads_label() ?></div>
          <div class="ad-responsive ad-mobile adsense-300">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- LP Mobile above share buttons at the bottom of post - 300x250 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-3062924748411310"
     data-ad-slot="4645285987"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
          </div>
        </div>

    <?php else: //パソコンの場合?>

        <div class="ad__space--bottom">
          <div class="ad-label"><?php echo get_ads_label() ?></div>
          <div class="ad-responsive adsense-336">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- LP Desktop Above Share buttons at the bottom of post -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-3062924748411310"
     data-ad-slot="8774434386"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
          </div>
        </div>

    <?php endif; ?>
<?php endif; ?>
