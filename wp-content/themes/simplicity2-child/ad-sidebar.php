<?php if ( is_ads_sidebar_enable() ): //広告をサイドバーに掲載するか?>
  <?php if (!is_mobile() ): ?>
    <div class="ad__space">
        <div class="ad-label"><?php echo get_ads_label() ?></div>
        <div class="ad-sidebar adsense-336">
          <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
          <!-- LP Desktop Right Top on Sidebar -->
          <ins class="adsbygoogle"
               style="display:inline-block;width:300px;height:250px"
               data-ad-client="ca-pub-3062924748411310"
               data-ad-slot="9053635985"></ins>
          <script>
          (adsbygoogle = window.adsbygoogle || []).push({});
          </script>
        </div>
    </div>
    <?php endif; ?>
<?php endif; ?>
