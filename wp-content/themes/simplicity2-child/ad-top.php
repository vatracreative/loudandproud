<div class="ad__space--article <?php echo (is_singular() ? ' ad-space-singular' : ''); ?>">
  <div class="ad-label"><?php echo get_ads_label() ?></div>

    <?php if ( is_mobile() ):?>
        <div class="<?php echo ( is_top_share_btns_visible() && is_single() ? ' ad-over-sns-buttons' : '' ); ?>">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
          <!-- LP Mobile Under H1 -->
          <ins class="adsbygoogle"
               style="display:inline-block;width:320px;height:100px"
               data-ad-client="ca-pub-3062924748411310"
               data-ad-slot="5064088383"></ins>
          <script>
          (adsbygoogle = window.adsbygoogle || []).push({});
          </script>
        </div>

    <?php else: ?>

        <div class="<?php echo ( is_top_share_btns_visible() && is_single() ? ' ad-over-sns-buttons' : '' ); ?>">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
          <!-- LP Desktop Under H1 - Rectangle -->
          <ins class="adsbygoogle"
               style="display:inline-block;width:336px;height:280px"
               data-ad-client="ca-pub-3062924748411310"
               data-ad-slot="4865611980"></ins>
          <script>
          (adsbygoogle = window.adsbygoogle || []).push({});
          </script>
        </div>

    <?php endif; ?>
    
</div>
