<?php //投稿本文 ?>
<?php //インデックス表示時はセクションで区切る（開始タグ）
//if ( !is_single() ) echo '<article>'; ?>
<?php //if ( is_single() ) get_template_part('edit-link'); //編集リンク?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <article class="<?php if ( !is_single() ) echo ' article-list'; ?>">
  <?php  if ( is_single() ) get_template_part('sns-buttons-sharebar'); //スクロール追従シェアバー ?>

  <?php //投稿本文上ウイジェット
  if ( is_single() && is_active_sidebar( 'widget-over-articletitle' ) ): ?>
    <?php dynamic_sidebar( 'widget-over-articletitle' ); ?>
  <?php endif; ?>

  <div class="entry__info">
    <?php
    if ( is_category_visible() && //カテゴリを表示する場合
             get_the_category() ):
      $cat = Array();
      $cat = get_the_category();
      $cat_name = $cat[0]->name;
      echo $cat_name;
    endif;
    ?>
    &nbsp;/&nbsp;<span class="fa fa-clock-o fa-fw"></span>
    <span class="entry__pubdate published">
    <?php
      the_date('Y.n.j');
    ?>
    </span>
    <?php
      $check_modified_date = get_the_time( 'U' ) !== get_the_modified_time( 'U' );
      if($check_modified_date) {
    ?>
    <span class="fa fa-history fa-fw"></span>
    <span class="entry__revidate date updated"><!-- "date updated" are needed for structured data -->
    <?php
        the_modified_date('Y.n.j');
      }
    ?>
    </span>
  </div>
  <?php if ( has_post_thumbnail() && is_eye_catch_visible() ): // サムネイルを持っているときの処理 ?>
    <figure class="eye-catch">
      <?php
          //the_post_thumbnail('large');
          //アイキャッチの表示
          the_post_thumbnail( 'thumb800', array('class' => 'entry__image', 'alt' => get_the_title()) );
      ?>
    </figure>
  <?php endif; ?>
    <h1 class="entry__title--post entry-title"><!-- "entry-title" is needed for structured data -->
      <?php if ( !is_single() ) echo '<a href="'.get_permalink().'">'; //投稿ページ以外ではタイトルにリンクを貼る?>
      <?php the_title(); //投稿のタイトル?>
      <?php if ( !is_single() ) echo '</a>'; //投稿ページ以外ではタイトルにリンクを貼る?>
    </h1>
    <?php get_template_part('admin-pv');//管理者のみにPV表示?>

    <?php if ( is_single() ) get_template_part('ad-top');//記事トップ広告 ?>

    <?php if ( is_single() ) get_template_part('sns-buttons-top');//タイトル下の小さなシェアボタン?>

    <?php //投稿本文上ウイジェット
    if ( is_single() && is_active_sidebar( 'widget-over-article' ) ): ?>
      <?php dynamic_sidebar( 'widget-over-article' ); ?>
    <?php endif; ?>




  <div id="the-content" class="entry-content">
  <?php //記事本文の表示
    the_content( get_theme_text_read_more() );

  ?>
  </div>

  <footer>
    <?php if ( is_single() ) get_template_part('pager-page-links');//ページリンクのページャー?>
    <?php //投稿本文下ウイジェット
    if ( is_single() && is_active_sidebar( 'widget-under-article' ) ): ?>
      <?php dynamic_sidebar( 'widget-under-article' ); ?>
    <?php endif; ?>

    <?php if ( is_single() ) get_template_part('ad-article-footer');?>

    <?php
      $photo_link = get_field('photo_link');
      $photo_photographer = get_field('photo_photographer');
      $photo_lincense = get_field('photo_license');
      if(!empty($photo_link)&&!empty($photo_photographer)) {
        ?>
      <div class="photo-credit">
        <a href="<?php echo $photo_link; ?>" target="_blank">Photo</a>&nbsp;By&nbsp;<?php echo $photo_photographer; ?>&nbsp;<a href="<?php echo $photo_lincense; ?>" target="_blank">CC</a>
      </div>
    <?php
      }
    ?>
    <div id="sns-group" class="sns-group sns-group-bottom">
    <?php if ( is_single() && is_bottom_share_btns_visible() )
      get_template_part('sns-buttons'); //SNSシェアボタンの取得?>

      <?php //投稿SNSボタン上ウイジェット
      if ( is_single() && is_active_sidebar( 'widget-over-sns-buttons' ) ): ?>
        <?php dynamic_sidebar( 'widget-over-sns-buttons' ); ?>
      <?php endif; ?>

    <?php if ( is_single() &&
               is_body_bottom_follows_visible() ) //記事下フォローボタン表示のとき
                 get_template_part('sns-pages'); //SNSフォーローボタンの取得?>
    </div>
    <?php //投稿SNSボタン下ウイジェット
    if ( is_single() && is_active_sidebar( 'widget-under-sns-buttons' ) ): ?>
      <div id="widget-under-sns-buttons" class="widgets">
      <?php dynamic_sidebar( 'widget-under-sns-buttons' ); ?>
      </div>
    <?php endif; ?>

    <p class="footer-post-meta">
      <?php if (is_tag_visible()): ?>
      <span class="post-tag"><?php the_tags('<span class="fa fa-tag fa-fw"></span>',', '); ?></span>
      <?php endif; ?>

      <?php if ( is_single() ) get_template_part('author-link') //投稿者リンク?>

      <?php if ( is_single() ) get_template_part('edit-link') //編集リンク?>

      <?php if ( is_single() ) wlw_edit_post_link('WLWで編集', '<span class="wlw-edit"><span class="fa fa-pencil-square-o fa-fw"></span>', '</span>'); ?>
    </p>
  </footer>
  </article><!-- .article -->
  <?php if ( is_list_style_bodies() ): //本文リストスタイルの時?>
  <hr class="sep" />
  <?php endif; ?>
</div><!-- .post -->
<?php //インデックス表示時はセクションで区切る（終了タグ）
//if ( !is_single() ) echo '</article>'; ?>
