<?php //投稿一覧リストのループ内で呼び出されるエントリーカード ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('entry__wrapper'.(is_list_style_large_thumb_cards() ? ' entry-large-thumbnail' : '').(is_list_style_tile_thumb_cards() ? ' entry-tile' : '').( is_entry_card_style() ? ' entry-card' : '')) ?>>
  <div class="entry__info">
    <?php
    if ( is_category_visible() && //カテゴリを表示する場合
             get_the_category() ):
      $cat = Array();
      $cat = get_the_category();
      $cat_name = $cat[0]->name;
      echo $cat_name;
    endif;
    ?>
    &nbsp;/&nbsp;<span class="fa fa-clock-o fa-fw"></span>
    <span class="entry__pubdate">
    <?php
      the_date('Y.n.j');
    ?>
    </span>
    <?php
      $check_modified_date = get_the_time('Y.n.j') < get_the_modified_time('Y.n.j');
      
      if($check_modified_date) {
    ?>
    <span class="fa fa-history fa-fw"></span>
    <span class="entry__revidate">
    <?php
        the_modified_date('Y.n.j');
      }
    ?>
    </span>
  </div>
  <a href="<?php the_permalink(); ?>" class="entry__anchor" title="<?php the_title(); ?>">
    <figure>
      <?php if ( is_entry_card_style() ): //デフォルトのサムネイルカード表示の場合?>
        <?php if ( has_post_thumbnail() ): // サムネイルを持っているとき ?>
          <?php the_post_thumbnail( 'thumb800', array('class' => 'entry__image', 'alt' => get_the_title()) ); ?>
        <?php else: // サムネイルを持っていない ?>
          <img src="<?php echo get_template_directory_uri(); ?>/images/no-image.png" alt="NO IMAGE" class="entry-thumnail no-image list-no-image entry__image" />
        <?php endif; ?>
      <?php endif; ?>
    </figure><!-- /.entry-thumb -->
    <h1 class="entry__title"><?php the_title(); ?></h1>
  </a>
  <?php get_template_part('admin-pv');//管理者のみにPV表示?>
  <p class="entry__description"><?php echo get_the_custom_excerpt( get_the_content(''), get_excerpt_length() ); //カスタマイズで指定した文字の長さだけ本文抜粋?></p>
</article>
