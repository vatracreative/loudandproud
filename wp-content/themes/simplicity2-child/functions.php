<?php //子テーマ用関数

//親skins の取得有無の設定
function include_parent_skins(){
  return true; //親skinsを含める場合はtrue、含めない場合はfalse
}

//子テーマ用のビジュアルエディタースタイルを適用
add_editor_style();

//Thumbnail size
add_image_size('thumb200', 200, 160, true);
add_image_size('thumb800', 800, 439, true);
//register_nav_menu( 'header-navi', 'ヘッダーナビゲーション' );
register_nav_menus(
  array(
    'mobile-navi' => 'モバイルナビ',
  )
);

function loudandproud_scripts() {
  wp_deregister_script('jquery');
  wp_register_script('jquery', "//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js", array(), null, true);
  wp_enqueue_script('jquery');
  wp_enqueue_script( 'jquery-migrate-js', "//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.min.js", array('jquery'), null, true);  
  wp_enqueue_script( 'loudandproud-js', get_stylesheet_directory_uri() . '/js/min/script.js', array( 'jquery' ), null, true);
}
add_action( 'wp_enqueue_scripts', 'loudandproud_scripts', 15 );


//Ad for mobile in Article
function showAdsInContent(){
//return'<br>ad space \' test<br>';
return '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- LP Mobile In Article - 300x200 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-3062924748411310"
     data-ad-slot="1552218783"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>';
}
add_shortcode('Ad Content', 'showAdsInContent');

function simplicity_scripts() {
  wp_enqueue_style( 'simplicity-style', get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'icomoon-style',  get_template_directory_uri() . '>/webfonts/icomoon/style.css', array('simplicity-style') );
  wp_enqueue_script( 'fontawesome', 'https://use.fontawesome.com/6280ac580f.js');
  wp_enqueue_style( 'child-style',  get_stylesheet_directory_uri() . '/style.css');
}
add_action( 'wp_enqueue_scripts', 'simplicity_scripts', 30 );