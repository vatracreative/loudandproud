<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MRW4SK8');</script>
<!-- End Google Tag Manager -->
<?php
//////////////////////////////////
//ウェブマスターツール用のID表示
//////////////////////////////////
if ( get_webmaster_tool_id() ): ?>
<meta name="google-site-verification" content="<?php echo get_webmaster_tool_id() ?>" />
<?php endif;//ウェブマスターツールID終了 ?>
<meta charset="<?php bloginfo('charset'); ?>">
<?php //ビューポート
//モバイルもしくはページキャシュモードの時
if ( is_mobile() || is_page_cache_enable() ): ?>
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
<?php else: ?>
  <meta name="viewport" content="width=1280, maximum-scale=1, user-scalable=yes">
<?php endif ?>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico?v=1.5">
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php //Wordpressのバージョンが4.1以下のとき
if ( floatval(get_bloginfo('version')) < 4.1 ):
  get_template_part('header-title-tag');
endif; ?>
<?php get_template_part('header-seo');//SEOの設定テンプレート?>
<?php //get_template_part('header-css');//CSS関連の記述まとめテンプレート?>
<?php //wp_enqueue_script('jquery');//jQueryの読み込み?>
<?php //get_template_part('header-css-mobile-responsive');//モバイル時、レスポンシブ時のCSS関連ファイル読み込みテンプレート（本来ならheader-css.phpに一つにまとめたいところだけど、このテンプレート（モバイル関連の記述）をここで読み込まないとモバイルで表示が崩れるサーバもあったので、あえて分けて書いてあります。?>
<?php //get_template_part('header-javascript');//JavaScript関連の記述まとめテンプレート?>
<?php the_apple_touch_icon_tag();//Appleタッチアイコンの呼び出し ?>
<?php if ( is_facebook_ogp_enable() ) //Facebook OGPタグ挿入がオンのとき
  get_template_part('header-ogp');//Facebook OGP用のタグテンプレート?>
<?php if ( is_twitter_cards_enable() ) //Twitterカードタグ挿入がオンのとき
  get_template_part('header-twitter-card');//Twitterカード用のタグテンプレート?>
<?php get_template_part('header-insert');//ユーザーが子テーマからヘッダーに何か記述したい時に利用するテンプレート?>
<?php get_template_part('head-custom-field');//カスタムフィールドの挿入（カスタムフィールド名：head_custom）?>
<?php wp_head(); ?>
<script>


</script>
</head>
  <body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MRW4SK8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
      <?php
      if(is_front_page()&&$paged==0) {
        echo '<div id="preloader" class="preloader__wrapper"></div>';
        get_template_part('hero');
        $header_class = 'header__inner--home';
      }
      else {
        $header_class = NULL;
      }
      ?>
      <!-- header -->
      <header itemscope itemtype="http://schema.org/WPHeader">
        <section id="header" class="header__inner <?php echo $header_class; ?>">

            <?php //カスタムヘッダーがある場合
            $h_top_style = '';
            if (get_header_image()){
              $h_top_style = ' style="background-image:url('.get_header_image().')"';
            } ?>

              <div class="blog__info__wrapper">
                <?php get_template_part('header-logo');?>
              </div>

              <div class="blog__menu">
                <?php if (is_navi_visible())://ナビゲーションが表示のとき
                  get_template_part('navi');//グローバルナビのためのテンプレート
                endif; ?>
                <?php //if ( is_top_follows_visible() ): //トップのフォローボタンを表示するか?>
                <?php //get_template_part('sns-pages'); //SNSフォローボタンの呼び出し?>
                <?php //endif; ?>

              </div>
              <?php get_template_part('mobile-nav'); ?>

        </section><!-- /#header -->
      </header>
        <?php
          if (!is_front_page()||!$paged==0) {
            if(!is_single()){
            get_template_part('hero-others');

            }
          }
        ?>
      <!-- 本体部分 -->
      <div id="body">
        <div  class="content__wrapper">

          <?php get_template_part('before-main'); //メインカラーの手前に挿入するテンプレート（3カラム作成カスタマイズ時などに） ?>

          <!-- main -->
          <main itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog" id="main" class="content__main">
