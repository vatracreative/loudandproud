<section class="hero" id="hero">
  <div class="hero__center">
    <h1 class="hero__center-title">Loud And Proud</h1>
    <div class="hero__center-description">海外就職、英語学習、Webに関する情報ブログ</div>
  </div>
</section>
