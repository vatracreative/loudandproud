( function( $ ) {
  var hero = $('#hero'),
      nav = $('#mobile__nav'),
      iconOpen = $('#mobile__nav__icon--open'),
      iconClose = $('#mobile__nav__icon--close');

  iconOpen.click(function(e) {
    e.preventDefault;
    nav.addClass('active');
    iconClose.addClass('active');
    return false;
  });

  iconClose.click(function(e) {
    e.preventDefault;
    nav.removeClass('active');
    iconClose.removeClass('active');
    return false;
  });

  setHeight();
  function setHeight(){
    var browserWidth = window.innerWidth,
        windowHeight = jQuery(window).innerHeight();//jQuery(window).heihgt();

    if(browserWidth < 768) {
     hero.css('height',windowHeight-60+"px");
    }
    else {
     hero.css('height',windowHeight-260+"px");
    }

  }

  $(window).resize(function(){
    setHeight();
  });

  $(window).load(function(){
    $('#preloader').fadeOut(1400,'linear');
  });

} )( jQuery );
