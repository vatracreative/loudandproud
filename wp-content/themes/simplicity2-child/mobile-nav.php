<!-- Navigation -->
<nav itemscope itemtype="http://schema.org/SiteNavigationElement" id="mobile__nav" class="mobile__nav">
  <a href="#" class="mobile__nav__icon--close__wrapper" id="mobile__nav__icon--close">
    <span class="mobile__nav__icon--close"></span>
  </a>
  <?php wp_nav_menu( array ( 'theme_location' => 'mobile-navi', 'menu_class' => 'mobile__nav__ul') ); ?>
</nav>
<!-- /Navigation -->
