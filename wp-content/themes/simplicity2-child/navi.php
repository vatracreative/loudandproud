<!-- Navigation -->
<div class="blog__menu__item">
  <a href="#" class="mobile__nav__icon--open__wrapper" id="mobile__nav__icon--open">
    <span class="mobile__nav__icon--open"></span>
  </a>
  <nav itemscope itemtype="http://schema.org/SiteNavigationElement" class="desktop__nav">
    <?php wp_nav_menu( array ( 'theme_location' => 'header-navi', 'menu_class' => 'desktop__nav__ul') ); ?>
  </nav>
</div>
<!-- /Navigation -->
